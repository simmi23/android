package com.example.simran.todoapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity
{
    RecyclerView recyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView=findViewById(R.id.rv_main);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        ToDoResponse sl=new ToDoResponse();
        sl.setTask("Wake Up");
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String data = df.format(new Date());
        sl.setDate(data);
        sl.setPriority("High");

        ToDoResponse s2=new ToDoResponse();
        s2.setTask("Have Breakfast");

        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
        String data1 = dateFormat.format(new Date());
        s2.setDate(data1);
        s2.setPriority("High");

        ArrayList<ToDoResponse> response=new ArrayList<>();
        response.add(sl);
        response.add(s2);

        ToDoAdapter toDoAdapter=new ToDoAdapter(response);
        recyclerView.setAdapter(toDoAdapter);
    }







}
