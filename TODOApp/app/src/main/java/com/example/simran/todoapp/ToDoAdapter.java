package com.example.simran.todoapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoAdapter.TodoViewHolder> {
    private ArrayList<ToDoResponse> responses;


    public ToDoAdapter(ArrayList<ToDoResponse> arrayList) {
        this.responses = arrayList;
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View singleLayoutView = layoutInflater.inflate(R.layout.single_ui, viewGroup, false);
        TodoViewHolder todoViewHolder = new TodoViewHolder(singleLayoutView);
        return todoViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder todoViewHolder, int i) {
        ToDoResponse singleResponse = responses.get(i);

        todoViewHolder.txtTask.setText(singleResponse.getTask());


      todoViewHolder.txtDate.setText(singleResponse.getDate());

      todoViewHolder.TxtPriority.setText(singleResponse.getPriority());



    }

    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class TodoViewHolder extends RecyclerView.ViewHolder {
        TextView txtTask, txtDate, TxtPriority;

        public TodoViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTask = itemView.findViewById(R.id.task_single_ui);
            txtDate = itemView.findViewById(R.id.date_single_ui);
            TxtPriority = itemView.findViewById(R.id.priority_single_ui);

        }
    }
}
