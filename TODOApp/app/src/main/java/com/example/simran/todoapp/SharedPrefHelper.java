package com.example.simran.todoapp;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefHelper
{
    public static final String USERNAME="username";

    private static  SharedPreferences getSharedPref(Context context)
    {
        return context.getSharedPreferences("mypref",context.MODE_PRIVATE);
    }


    public static void setUsername(Context context,String username)
    {
        getSharedPref(context).edit().putString(USERNAME,username).commit();
    }


    public static  String getUsername(Context context)
    {
       return getSharedPref(context).getString(USERNAME,"");
    }



    /*  private void login()
    {
        SharedPreferences sharedPreferences=getSharedPreferences("mypres",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("username","simmi");
        editor.commit();

        SharedPrefHelper.setUsername(this,"simmi");

        on main activity
    }*/

}
