package com.example.simran.animatelogin;

import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;


public class MainActivity extends AppCompatActivity
{

    AnimationDrawable animationDrawable;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar=findViewById(R.id.toolbar_activity_main);
        setSupportActionBar(toolbar);

        FrameLayout frameLayout=findViewById(R.id.fl_content_layout);
        animationDrawable=(AnimationDrawable) frameLayout.getBackground();

        animationDrawable.setEnterFadeDuration(5000);
        animationDrawable.setExitFadeDuration(2000);

        animationDrawable.start();
    }
}
