package com.example.simran.contactapp;



import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "tab_contacts")
public class ContactEntity
{
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String contactId;

    @ColumnInfo(name = "name")
    private String contactName;

    @ColumnInfo(name = "first_number")
    private String firstNumber;

    @ColumnInfo(name = "email")
    private String contactEmail;

    // Constructor
    public ContactEntity(String contactId, String contactName, String firstNumber, String contactEmail)
    {
        this.contactId = contactId;
        this.contactName = contactName;
        this.firstNumber = firstNumber;
        this.contactEmail = contactEmail;
    }

    // Getters
    public String getContactId()
    {
        return contactId;
    }

    public String getContactName()
    {
        return contactName;
    }

    public String getFirstNumber()
    {
        return firstNumber;
    }

    public String getContactEmail()
    {
        return contactEmail;
    }
}
