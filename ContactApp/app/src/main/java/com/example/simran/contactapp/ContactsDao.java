package com.example.simran.contactapp;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ContactsDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long saveContact(ContactEntity contact);

    @Query("SELECT * FROM tab_contacts")
    List<ContactEntity> getAllContacts();
}