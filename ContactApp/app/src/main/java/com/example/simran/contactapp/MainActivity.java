package com.example.simran.contactapp;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.support.v7.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;


import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ContactsAdapter.ContactsAdapterListener
{
    private SearchView searchView;
    List<ContactEntity> allContacts;
    ContactsAdapter mAdapter;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    int Perresult=0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.toolbar_activity_main);

        recyclerView = findViewById(R.id.rv_activity_main);
        progressBar=findViewById(R.id.progress_bar_activity_main);

        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#000000"));

        String[] permission = {Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS};
        Perresult = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_CONTACTS);

        if (Perresult == PackageManager.PERMISSION_DENIED)
        {
            //Request for permission
            ActivityCompat.requestPermissions(MainActivity.this,
                    permission,
                    3131);

        }

        /*if(ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.READ_CONTACTS)!=PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_CONTACTS))
            {
                AlertDialog.Builder builder= new AlertDialog.Builder(this);
                builder.setMessage("Do you want to End game");
                builder.setCancelable(true);

                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                Toast.makeText(MainActivity.this, "NO", Toast.LENGTH_SHORT).show();
                            }
                        });


                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert11;
                alert11 = builder.create();
                alert11.show();


            }
            else
                {
                ActivityCompat.requestPermissions(MainActivity.this, permission, 3131);
            }
        }
        else
        {*/
            ReadContacts readContacts = new ReadContacts();
            readContacts.execute();

            GetAllContacts getAllContacts = new GetAllContacts();
            getAllContacts.execute();






        allContacts = new ArrayList<>();




        mAdapter= new ContactsAdapter(MainActivity.this,allContacts, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        //recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onContactSelected(ContactEntity contact)
    {
        Toast.makeText(getApplicationContext(), "Selected: " + contact.getContactName() + ", " + contact.getFirstNumber(), Toast.LENGTH_LONG).show();
    }


    private class GetAllContacts extends AsyncTask<Void, Void, List<ContactEntity>>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<ContactEntity> doInBackground(Void... voids) {

            List<ContactEntity> allContacts = AppDatabase.getInstance(MainActivity.this).getContactsDao().getAllContacts();

            return allContacts;
        }

        @Override
        protected void onPostExecute(List<ContactEntity> contactEntities) {
            super.onPostExecute(contactEntities);

            progressBar.setVisibility(View.INVISIBLE);
            if (contactEntities!= null)
            {
                allContacts.addAll(contactEntities);
                mAdapter.notifyDataSetChanged();
            }
        }
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);



            MenuItem menuItem = menu.findItem(R.id.search_menu_search);
            //materialSearchView.setMenuItem(menuItem);

            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) menuItem.getActionView();
             changeSearchViewTextColor(searchView);
            searchView.setSearchableInfo(searchManager
                    .getSearchableInfo(getComponentName()));
            searchView.setMaxWidth(Integer.MAX_VALUE);
            searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query)
                {
                   mAdapter.getFilter().filter(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    mAdapter.getFilter().filter(newText);
                    return true;
                }
            });



        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    //AsynTask
    private class ReadContacts extends AsyncTask<Void, Void, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {

           int result1 = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_CONTACTS);
            String[] permission = {Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS};
            if (result1 == PackageManager.PERMISSION_DENIED) {
                //Request for permission
                ActivityCompat.requestPermissions(MainActivity.this,
                        permission,
                        3131);
            }


                ContentResolver contentResolver = getContentResolver();
                Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

                while (cursor != null && cursor.moveToNext()) {
                    // ContactEntity contactEntity=new ContactEntity();

                    String id;
                    String name = null;
                    String email = null;
                    String number = null;


                    id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    //String email=cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.D))


                    Cursor phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);

                    Log.d("TAG", id + " " + name);

                    while (phoneCursor != null && phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        number = phoneNumber;

                        // Log.d("TAG",id+ " "+ phoneNumber);
                    }

                    Cursor emailCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + "= ?",
                            new String[]{id},
                            null);

                    while (emailCursor != null && emailCursor.moveToNext()) {
                        String emailAddress = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        email = emailAddress;

                        // Log.d("TAG", emailAddress);
                        // stringBuilder.append(emailAddress + "\n--------------------------\n");
                    }

                    ContactEntity contactEntity = new ContactEntity(id, name, number, email);
                    //save to sqlite using room
                    AppDatabase appDatabase = AppDatabase.getInstance(MainActivity.this);
                    ContactsDao contactsDao = appDatabase.getContactsDao();

                    long result = contactsDao.saveContact(contactEntity);

                 /*AppDatabase appDatabase1=AppDatabase.getInstance(MainActivity.this);
                 ContactsDao contactsDao1=appDatabase1.getContactsDao();
                 List<ContactEntity> allContacts = contactsDao1.getAllContacts();*/

                    Log.d("Contacts", result > 0 ? "Insertion Successful" : "Failed to insert");
                }




            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }



    private void changeSearchViewTextColor(View view)
    {
        if(view != null)
        {
            if(view instanceof TextView)
            {
                ((TextView) view).setTextColor(Color.WHITE);
                return;
            }
            else if(view instanceof ViewGroup)
            {
                ViewGroup viewGroup=(ViewGroup) view;
                for(int i=0;i<viewGroup.getChildCount();i++)
                {changeSearchViewTextColor(viewGroup.getChildAt(i));}
            }
        }
    }
}