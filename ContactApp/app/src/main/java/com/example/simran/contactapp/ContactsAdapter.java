package com.example.simran.contactapp;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.lang.CharSequence;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<ContactEntity> contactList;
    private List<ContactEntity> contactListFiltered;
    private ContactsAdapterListener listener;




    /*public ContactsAdapter(Context context, List<ContactEntity> contactList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.contactList = contactList;
        contactListFiltered = new ArrayList<>(contactList);
    }
*/
    public ContactsAdapter(Context context, List<ContactEntity> contactList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.contactList = contactList;
        contactListFiltered = new ArrayList<>(contactList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View singleLayoutView= layoutInflater.inflate(R.layout.single_ui,parent,false);
        MyViewHolder myViewHolder= new MyViewHolder(singleLayoutView);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {
        ContactEntity contactEntity= contactList.get(position);

        TextView txt_name= holder.txtName;
        txt_name.setText(contactEntity.getContactName());
        txt_name.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                CustomDialogClass customDialogClass=new CustomDialogClass(context,contactEntity.getContactName(),contactEntity.getFirstNumber(),contactEntity.getContactEmail());

                customDialogClass.show();

            }
        });




        Log.d("Tag","values of position I:"+position);



    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence)
            {
                List<ContactEntity> filteredList = new ArrayList<>();
//                String charString = charSequence.toString();
                if (charSequence==null || charSequence.length()==0) {
                    filteredList.addAll(contactListFiltered);
                } else {
                       String filterpattern=charSequence.toString().toLowerCase().trim();
                    for (ContactEntity row : contactListFiltered) {


                        if (row.getContactName().toLowerCase().contains(filterpattern))
                        {
                            filteredList.add(row);
                        }
                    }



                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //contactList.clear();
                contactList.addAll((List)filterResults.values);
                notifyDataSetChanged();
            }
        };

    }
    public interface ContactsAdapterListener {
        void onContactSelected(ContactEntity contact);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.txt_name_single_ui)
        TextView txtName;

        public MyViewHolder(View view)
        {
            super(view);
            ButterKnife.bind(this,view);

            view.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View view)
                {
                    // send selected contact in callback
                    listener.onContactSelected(contactListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }



    public class CustomDialogClass extends Dialog
    {


        public Activity c;
        public Dialog d;
        public TextView txtName,txtNumber,txtEmail;
        //View view1=findViewById(R.id.view_single_ui);


        public CustomDialogClass(@NonNull Context context, String n, String e, String no )
        {
            super(context);

            setContentView(R.layout.custom_layout);

            txtName=findViewById(R.id.txt_name_custom_layout);
            txtName.setText(n);
            txtEmail=findViewById(R.id.txt_email_custom_layout);
            txtEmail.setText(e);
            txtNumber=findViewById(R.id.txt_number_custom_layout);
            txtNumber.setText(no);






        }

    }
}