package com.example.simran.contactapp;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {ContactEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase
{
    private static final String DATABASE_NAME = "contacts.db";
    private static AppDatabase INSTANCE;

    // Dao method
    public abstract ContactsDao getContactsDao();

    public static AppDatabase getInstance(Context context)
    {
        if (INSTANCE == null)
        {
            INSTANCE = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME)
                    .build();
        }
        return INSTANCE;
    }
}