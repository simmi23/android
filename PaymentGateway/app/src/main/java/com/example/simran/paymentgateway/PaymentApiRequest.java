package com.example.simran.paymentgateway;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PaymentApiRequest {
    @FormUrlEncoded
    @POST("paytm/paytm_php_api/generateChecksum.php")
    Call<CheckSumResponse> getChecksum(
            @Field("ORDER_ID") String Order_id,
            @Field("CUST_ID") String customer_id,
            @Field("MOBILE_NO") String mobile_number,
            @Field("TXN_AMOUNT") String transaction_amount,
            @Field("EMAIL") String email_id
    );
}
