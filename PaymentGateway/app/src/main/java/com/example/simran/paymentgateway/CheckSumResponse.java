package com.example.simran.paymentgateway;

import com.google.gson.annotations.SerializedName;

public class CheckSumResponse
{
    @SerializedName("CHECKSUMHASH")
    private String checksumHash;

    @SerializedName("ORDER_ID")
    private  String orderId;

    @SerializedName("payt_STATUS")
    private  String paytmstatus;

    //getters and setters

    public String getChecksumHash() {
        return checksumHash;
    }

    public void setChecksumHash(String checksumHash) {
        this.checksumHash = checksumHash;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaytmstatus() {
        return paytmstatus;
    }

    public void setPaytmstatus(String paytmstatus) {
        this.paytmstatus = paytmstatus;
    }
}
