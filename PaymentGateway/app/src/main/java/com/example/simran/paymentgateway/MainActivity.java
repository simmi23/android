package com.example.simran.paymentgateway;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import java.util.HashMap;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "TAG";
    public static final String CUSTOMER_ID = "custId3131";
    public static final String MOBILE_NUMBER = "9422496835";
    public static final String EMAIL_ID = "simrannagdeo23@gmail.com";
    public static final String TRANSACTION_AMOUNT = "2.00";
    private String orderId;
    private String checksum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        orderId = "order1xxx" + new Random().nextInt(9999);

        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create();

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl("https://kothrud.000webhostapp.com/");
        retrofitBuilder.addConverterFactory(gsonConverterFactory);

        Retrofit retrofit = retrofitBuilder.build();

        PaymentApiRequest paymentApiRequest = retrofit.create(PaymentApiRequest.class);
        Call<CheckSumResponse> responseCall = paymentApiRequest.getChecksum(orderId, CUSTOMER_ID, MOBILE_NUMBER, TRANSACTION_AMOUNT, EMAIL_ID);

        responseCall.enqueue(new Callback<CheckSumResponse>() {
            @Override
            public void onResponse(Call<CheckSumResponse> call, Response<CheckSumResponse> response) {
                if (response.body() == null)
                    return;

                if (response.body().getPaytmstatus().equals("1")) {
                    checksum = response.body().getChecksumHash();
                    Toast.makeText(MainActivity.this, checksum, Toast.LENGTH_SHORT).show();

                    Log.d("CheckSum", "onResponse: " + checksum);
                }

            }

            @Override
            public void onFailure(Call<CheckSumResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        Button btnClickOnPaytm = findViewById(R.id.btn_hello_activity_main);
        btnClickOnPaytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaytmPGService paytmPGService = PaytmPGService.getStagingService();

                if (checksum == null) {
                    Toast.makeText(MainActivity.this, "Checksum not generated. Please Wait", Toast.LENGTH_SHORT).show();
                    return;
                }

                //Order Object
                HashMap<String, String> paramMap = new HashMap<>();

                //merchant id
                paramMap.put("MID", "YNpdZn71044630530535");

                //order id
                paramMap.put("ORDER_ID", orderId);

                //Customer id
                paramMap.put("CUST_ID", CUSTOMER_ID);

                //MOBILE NUMBER
                paramMap.put("MOBILE_NO", MOBILE_NUMBER);

                //Email
                paramMap.put("EMAIL", EMAIL_ID);

                //CHANNEL ID
                paramMap.put("CHANNEL_ID", "WAP");

                //TRANSACTION AMOUNT
                paramMap.put("TXN_AMOUNT", TRANSACTION_AMOUNT);

                //websatging
                paramMap.put("WEBSITE", "WEBSTAGING");

                //satging value
                paramMap.put("INDUSTRY_TYPE_ID", "Retail");

                //callbackURL
                paramMap.put("CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId);

                //CHECHSUM HASH
                paramMap.put("CHECKSUMHASH", checksum);

                PaytmOrder paytmOrder = new PaytmOrder(paramMap);

                paytmPGService.initialize(paytmOrder, null);

                paytmPGService.startPaymentTransaction(MainActivity.this, true/*paytm bar will not be displayed*/,
                        true/*bind checksum*/,
                        new PaytmPaymentTransactionCallback() {
                            @Override
                            public void onTransactionResponse(Bundle inResponse) {
                                String status = inResponse.getString("STATUS");

                                if (status.equals("TXN_SUCCESS")) {
                                    Toast.makeText(MainActivity.this, "payment successful", Toast.LENGTH_SHORT).show();
                                } else if (status.equals("TXN_FAILURE")) {
                                    Toast.makeText(MainActivity.this, "Payment Failed", Toast.LENGTH_SHORT).show();
                                }

                                Toast.makeText(MainActivity.this, "paytm transaction response" + inResponse.toString(), Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "onTransactionResponse" + inResponse.toString());
                            }

                            @Override
                            public void networkNotAvailable() {
                                Toast.makeText(MainActivity.this, "No Internet", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void clientAuthenticationFailed(String inErrorMessage) {
                                Toast.makeText(MainActivity.this, "Auth Failed. Server Error:" + inErrorMessage.toString(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void someUIErrorOccurred(String inErrorMessage) {
                                Toast.makeText(MainActivity.this, "someUIErrorOccurred" + inErrorMessage, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                                Toast.makeText(MainActivity.this, "onErrorLoadingWebPage" + inErrorMessage, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onBackPressedCancelTransaction() {
                                Toast.makeText(MainActivity.this, "You back pressed the transaction", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                                Toast.makeText(MainActivity.this, "ztransaction cancelled" + inErrorMessage, Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

    }
}
