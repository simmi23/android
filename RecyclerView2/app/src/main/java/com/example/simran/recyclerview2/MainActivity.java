package com.example.simran.recyclerview2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
{
         @BindView(R.id.rv_activity_main)
         RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Butterknife
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(MainActivity.this);

        recyclerView.setLayoutManager(linearLayoutManager);

        Data first = new Data();
        first.setTitle("Ali Connors");
        first.setSub_title("Brunch This Weekend?");
        first.setDescription("i'll be in your neighbourhood during errands...");
        first.setTime("15h");

        Data second = new Data();
        second.setTitle("me,scott,jennifer");
        second.setSub_title("Summer BBQ");
        second.setDescription("Wish i Could, but i m out of town");
        second.setTime("2h");

        Data third = new Data();
        third.setTitle("Sandra adams");
        third.setSub_title("Oui Oui");
        third.setDescription("Do u have Paris Recommendations?...");
        third.setTime("6h");

      Data fourth= new Data();
        fourth.setTitle("Trevor Hansen");
        fourth.setSub_title("Order Confirmed");
        fourth.setDescription("Thank you for your Recent Order.....");
        fourth.setTime("12h");

       Data fifth= new Data();
        fifth.setTitle("Britta Holt");
        fifth.setSub_title("Recipe to try");
        fifth.setDescription("we should eat this:Grated squash,corn...");
        fifth.setTime("18h");

        List<Data> dataList=new ArrayList<>();
        dataList.add(first);
        dataList.add(second);
        dataList.add(third);
        dataList.add(fourth);
        dataList.add(fifth);


        DataAdapter dataAdapter= new DataAdapter(dataList);
        recyclerView.setAdapter(dataAdapter);

    }
}
