package com.example.simran.recyclerview2;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder>
{
     private final static String TAG="DataAdapter";
    private List<Data> dataKiList;

    public DataAdapter(List<Data>dataList)
    {
        this.dataKiList = dataList;

    }
    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        LayoutInflater layoutInflater=LayoutInflater.from(viewGroup.getContext());
        View singleLayoutView= layoutInflater.inflate(R.layout.single_layout_file,viewGroup,false);
        DataViewHolder dataViewHolder= new DataViewHolder(singleLayoutView);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder dataViewHolder, int position)
    {
        Data singleLayout= dataKiList.get(position);
        TextView txt_title= dataViewHolder.text_title;
        txt_title.setText(singleLayout.getTitle());

        TextView txt_sub_title= dataViewHolder.textsub_title;
        txt_sub_title.setText(singleLayout.getSub_title());

        TextView txt_desc= dataViewHolder.text_description;
        txt_desc.setText(singleLayout.getDescription());

        TextView txt_time= dataViewHolder.text_time;
        txt_time.setText(singleLayout.getTime());

        Log.d(TAG,"values of position I:"+position);


    }

    @Override
    public int getItemCount()

    {
        return dataKiList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder

    {


        @BindView(R.id.txt_title_single_layout_file)
        TextView text_title;

        @BindView(R.id.txt_sub_title_single_layout_file)
        TextView textsub_title;

        @BindView(R.id.txt_description_single_layout_file)
        TextView text_description;

        @BindView(R.id.txt_time_single_layout_file)
        TextView text_time;

        public DataViewHolder(@NonNull View itemView)
        {
            super(itemView);

            //butterknife
            ButterKnife.bind(this,itemView);
        }


    }
}
