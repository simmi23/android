package com.example.simran.currentlocation;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.simran.fragment.Entity.ContactSelectedEntity;
import com.example.simran.fragment.dao.saveSelectedContacts;
import com.example.simran.fragment.database.SaveContacts;

import java.util.ArrayList;
import java.util.List;

public class FragmentContact extends Fragment {
    private static final int CONTACT_REQUEST_CODE = 1;
    View view;
    List<String> contactName;
    List<String> contactNumber;
    ContactSelectedEntity contactSelectedEntity;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    private RecyclerView recyclerView;
    private List<ContactEntity> contactEntities = new ArrayList<>();
    private ProgressBar progressBar;
    private ContactEntity contactEntity;
    Context thisContext;
    ContactsAdapter contactsAdapter;


    private FloatingActionButton floatingActionButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.contact, container, false);
        thisContext = container.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(thisContext);

        BindViews();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        contactsAdapter = new ContactsAdapter(contactEntities);
        recyclerView.setAdapter(contactsAdapter);
        // ReadContacts readContacts=new ReadContacts();
        // readContacts.execute();

        saveSelectedContacts();


        /*if (ContextCompat.checkSelfPermission(thisContext, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_CONTACTS)) {


                builder
                        .setTitle("Permission Needed")
                        .setMessage("This Permission is needed to load the contacts")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS}, CONTACT_REQUEST_CODE);
                               // ReadContacts readContacts = new ReadContacts();
                               // readContacts.execute();
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });


                AlertDialog alert11;
                alert11 = builder.create();
                alert11.show();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS}, CONTACT_REQUEST_CODE);
            }

        } else {
            ReadContacts readContacts = new ReadContacts();
            readContacts.execute();
        }
*/
        ReadContacts readContacts = new ReadContacts();
        readContacts.execute();

        return view;
    }


    private void BindViews() {
        progressBar = view.findViewById(R.id.pg_fragmentcontact);
        floatingActionButton = view.findViewById(R.id.addContact_contact_activity);
        recyclerView = view.findViewById(R.id.recycler_view_contacts);
        contactName = new ArrayList<String>();
        contactNumber = new ArrayList<String>();

    }

    private void saveSelectedContacts() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "";
                String number = "";
                for (ContactEntity contactEntity : contactEntities) {
                    if (contactEntity.isSelected()) {
                        contactName.add(contactEntity.getContactName());
                        contactNumber.add(contactEntity.getFirstNumber());
                        //text += contactEntity.getContactName();
                        //number+=contactEntity.getFirstNumber();
                        int x = contactName.size();
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                for (int i = 0; i < x; i++) {
                                    contactSelectedEntity = new ContactSelectedEntity(contactName.get(i), contactNumber.get(i));
                                }

                                SaveContacts notesdatabase = SaveContacts.getInstance(getContext());

                                saveSelectedContacts notesdao = notesdatabase.contactdao();
                                final long result = notesdao.saveNotes(contactSelectedEntity);

                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        if (result > 0) {

                                            Toast.makeText(getContext(), "contact saved", Toast.LENGTH_LONG).show();
                                            //notesAdapter = new NotesAdapter(notesEntities);
                                            //recyclerView.setAdapter(notesAdapter);
                                        }
                                    }
                                });
                            }
                        });
                        thread.start();
                    }

                }


            }
        });
    }

    public class ReadContacts extends AsyncTask<Void, Void, List<ContactEntity>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<ContactEntity> doInBackground(Void... voids) {

            Context applicationContext = MainActivity.getContextOfApplication();


            ContentResolver contentResolver = applicationContext.getContentResolver();
            Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);


            while (cursor != null && cursor.moveToNext()) {
                String id;
                String name = null;
                String email = null;
                String phoneNumber = null;
                contactEntity = new ContactEntity();


                id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                contactEntity.setContactId(id);
                contactEntity.setContactName(name);


                Cursor phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);

                while (phoneCursor != null && phoneCursor.moveToNext()) {
                    phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    //number = phoneNumber;
                    contactEntity.setFirstNumber(phoneNumber);
                }

                Cursor emailCursor = contentResolver.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + "= ?",
                        new String[]{id},
                        null);

                while (emailCursor != null && emailCursor.moveToNext()) {
                    String emailAddress = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    email = emailAddress;
                    contactEntity.setContactEmail(email);


                }

                contactEntities.add(contactEntity);


            }


            return contactEntities;
        }

        @Override
        protected void onPostExecute(List<ContactEntity> dataList) {
            super.onPostExecute(dataList);
            progressBar.setVisibility(View.INVISIBLE);
            if (dataList != null) {

                contactsAdapter = new ContactsAdapter(dataList);
                recyclerView.setAdapter(contactsAdapter);
                Log.d("TAG", "name= " + dataList);


            }
        }
    }

}