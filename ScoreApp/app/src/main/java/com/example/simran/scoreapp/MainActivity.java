package com.example.simran.scoreapp;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;


public class MainActivity extends AppCompatActivity
{
    public int count1=0;
    public int count2=0;
    public   TextView textCount1;
    public   TextView textCount2;
    public Button btnPlusPl1;
    public   Button btnMinuspl1;
    public Button btnPlusPl2;
    public   Button btnMinuspl2;


    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        onPlusOfCount1();
        onMinusOfCount1();
        onPlusOfCount2();
        onMinusOfCount2();
        print();

    }

    private void bindViews()
    {
         textCount1=findViewById(R.id.txt_count1_activity_main);
         textCount2=findViewById(R.id.txt_count2_activity_main);
         btnPlusPl1=findViewById(R.id.btn_plus1_activity_home);
         btnMinuspl1=findViewById(R.id.btn_minus1_activity_home);
         btnPlusPl2=findViewById(R.id.btn_plus2_activity_home);
         btnMinuspl2=findViewById(R.id.btn_minus2_activity_home);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_game, menu);
        // getMenuInflater().inflate(R.menu_game.menu_game,menu_game);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        int id= item.getItemId();

        switch(id)
        {
            case R.id.Reset_menu_game:
                //Toast.makeText(this, "Reset Selected", Toast.LENGTH_SHORT).show();
              if(item.getTitle().equals("reset"))
              {
                  count1=0;
                  count2=0;
                  textCount1.setText(String.valueOf(count1));
                  textCount2.setText(String.valueOf(count2));
                  SprefClear();


              }
              break;

            case R.id.end_game_menu:
                //Toast.makeText(this, "End Game", Toast.LENGTH_SHORT).show();
               if(item.getTitle().equals("EndGame"))
               {
                        dialogBox();
               }

                break;


        }
        return true;
    }

    private void onPlusOfCount1()
    {
        btnPlusPl1.setOnClickListener
                (new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                count1++;
                textCount1.setText(String.valueOf(count1));
                sharedPref();

            }
        });

    }



    private void onMinusOfCount1()
    {
        btnMinuspl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                count1--;
                textCount1.setText(String.valueOf(count1));
                sharedPref();

            }
        });

    }

    private  void  onPlusOfCount2()
    {
        btnPlusPl2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                count2++;
                textCount2.setText(String.valueOf(count2));
                sharedPref();

            }
        });
    }

    private  void  onMinusOfCount2()
    {
        btnMinuspl2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                count2--;
                textCount2.setText(String.valueOf(count2));
                sharedPref();

            }
        });
    }

        public void dialogBox()
        {
            AlertDialog.Builder builder= new AlertDialog.Builder(this);
            builder.setMessage("Do you want to End game");
            builder.setCancelable(true);

            builder.setPositiveButton("Yes",

                    new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                   if(count1>count2)
                   {
                       Toast.makeText(MainActivity.this, "player 1 Wins", Toast.LENGTH_SHORT).show();
                   }
                   else if(count2>count1) {
                       Toast.makeText(MainActivity.this, "Player 2 Wins", Toast.LENGTH_SHORT).show();
                   }
                   else if(count1 == count2)
                   {
                       Toast.makeText(MainActivity.this, "Tie between the players", Toast.LENGTH_SHORT).show();
                   }

                    finish();

                }
            });


            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alert11;
            alert11 = builder.create();
            alert11.show();
        }






        //shared pref
        public void sharedPref()
        {
           String count11 = textCount1.getText().toString();
            String count12 = textCount2.getText().toString();
            SharedPreferences sharedPref = getSharedPreferences("Score", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("count1", count11);
            editor.putString("count2",count12);
            editor.apply();
        }

        public void print()
        {
            SharedPreferences sharedPref = getSharedPreferences("Score", Context.MODE_PRIVATE);
            String count11 = sharedPref.getString("count1", "");
            String count12 = sharedPref.getString("count2","");
            textCount1.setText(count11);
            textCount2.setText(count12);

        }

        public void SprefClear()
        {
            SharedPreferences sharedPref = getSharedPreferences("Score", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.clear();
            editor.apply();
        }


}

