package com.example.simran.mykothrudcity.client;


import com.example.simran.mykothrudcity.model.AllCategoryModel;
import com.example.simran.mykothrudcity.model.ListingsModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MyCityKothrudClient
{

  @GET("kothrud/api/get_all_category.php")
  Call<AllCategoryModel> getAllCategories();

  @FormUrlEncoded
  @POST("kothrud/api/get_lis_by_cat.php")
  Call<ListingsModel> getListings(
         @Field("key_cat_id") String categoryId
  );


}
