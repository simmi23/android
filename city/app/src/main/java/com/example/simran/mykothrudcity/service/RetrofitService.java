package com.example.simran.mykothrudcity.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService
{
    private Retrofit retrofit;

    //constructor
    public RetrofitService()
    {
        Gson gson = new GsonBuilder().serializeNulls().create();
        GsonConverterFactory factory= GsonConverterFactory.create(gson);

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl("https://kothrud.000webhostapp.com/");
        retrofitBuilder.addConverterFactory(factory);

         retrofit = retrofitBuilder.build();
    }
    //Getter method for retrofit
    public Retrofit getRetrofit()
    {
        return retrofit;
    }
}
