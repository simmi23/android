package com.example.simran.mykothrudcity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class NewsActivity extends AppCompatActivity
{
        private WebView webView;
        private ProgressBar progressbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        String urlToload="https://www.google.com/";
        webView=findViewById(R.id.webView_activity_news);

        //XSS-cross site scripting(problem in javascript:hacker attack)
        //so javascript is not enabled ,we have to enable it
        WebSettings webSettings=webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(urlToload);

        //problems
        //1.No progress Indicator
        //2.Doesn't update pageTitle on actionbar
        //3.No Back History
        //4.No Javascript Enabled

        //settings of webview(for javascript enabling)


        progressbar=findViewById(R.id.progress_activity_news);
        //To load your web pages in your app itself use WebViewClient
        WebViewClient webViewClient=new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                super.onPageStarted(view, url, favicon);
                //show the progress bar
                progressbar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);
                //hide the progress bar
                progressbar.setVisibility(View.INVISIBLE);
                //get the pagetitle of current web page
                String pageTitle=view.getTitle();
                //SET the pagetitle to action bar

               /* //to chamge the color of action bar when it will to new page
                ColorDrawable colorDrawable=new ColorDrawable();
                colorDrawable.setColor(Color.CYAN);
                getSupportActionBar().setBackgroundDrawable(colorDrawable);*/
                getSupportActionBar().setTitle(pageTitle);
                getSupportActionBar().setSubtitle(url);
            }
        };
        //Attach webView to WebViewClient
        webView.setWebViewClient(webViewClient);
    }

    @Override
    public void onBackPressed() {

        boolean cangoback=webView.canGoBack();
        if(cangoback)
        {
            webView.goBack();
        }
        else
        {
            //super==finish
            //finishAffinity clears stack memory and closed the previous methods
            super.onBackPressed();
        }
    }
}
