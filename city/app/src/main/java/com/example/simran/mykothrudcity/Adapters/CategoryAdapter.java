package com.example.simran.mykothrudcity.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.simran.mykothrudcity.ListingsActivity;
import com.example.simran.mykothrudcity.R;
import com.example.simran.mykothrudcity.model.AllCategoryModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>
{


    private Context contextA;
    private List<AllCategoryModel.Category>  categoryList;
    public CategoryAdapter(Context context,List<AllCategoryModel.Category> categoryList)
    {
        this.contextA = context;
        this.categoryList=categoryList;
    }
    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
    {
       LayoutInflater layoutInflater= LayoutInflater.from(viewGroup.getContext());
       View singleItemView= layoutInflater.inflate(R.layout.single_category_ui,viewGroup,false);



       CategoryViewHolder categoryViewHolder= new CategoryViewHolder(singleItemView);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int position)
    {
       AllCategoryModel.Category category= this.categoryList.get(position);
       String categoryId= category.getId();
       String categoryName=category.getCategoryName();

        categoryViewHolder.txtCategoryName.setText(categoryName);

        //load image via picasso
        categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
              //starts Listings Activity

                Intent listingIntent=new Intent(contextA,ListingsActivity.class);
                listingIntent.putExtra("category_id_key",categoryId);
                listingIntent.putExtra("category_title_key",categoryName);
                contextA.startActivity(listingIntent);

            }
        });

    }

    @Override
    public int getItemCount()
    {
        return this.categoryList.size();
    }


    //Inner Class for RecyclerView's ViewHolder
    public class CategoryViewHolder extends  RecyclerView.ViewHolder
    {
        @BindView(R.id.txt_cat_single_category_ui)
        TextView txtCategoryName;

        @BindView(R.id.img_cat_single_category_ui)
        ImageView imgCategoryUrl;

        //constructor
        public CategoryViewHolder(@NonNull View itemView)
        {
            super(itemView);

            //Butterknife
            ButterKnife.bind(this,itemView);
        }

    }
}
