package com.example.simran.mykothrudcity.model;

import android.icu.util.ULocale;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Locale;

public class AllCategoryModel
{
    //@SerializedName(value="Poster" , alternate = "poster ")

    @SerializedName("success")
    private boolean success;

    @SerializedName("message")
    String message;

    @SerializedName("categories")
    private List<Category> categoryList;

    //Inner Static class for single category
    public static class Category
    {
        @SerializedName("cat_id")
        public String id;

        @SerializedName("category_name")
        private String categoryName;

        @SerializedName("cat_image_url")
        private String category_img_url;

        //Getter and setters for category


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getCategory_img_url() {
            return category_img_url;
        }

        public void setCategory_img_url(String category_img_url) {
            this.category_img_url = category_img_url;
        }
    }

    //Getter and setters for AllCategoryModel


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
