package com.example.simran.mykothrudcity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simran.mykothrudcity.Adapters.CategoryAdapter;
import com.example.simran.mykothrudcity.client.MyCityKothrudClient;
import com.example.simran.mykothrudcity.model.AllCategoryModel;
import com.example.simran.mykothrudcity.service.RetrofitService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class CategoryActivity extends AppCompatActivity
{
    @BindView(android.R.id.content)
    View currentView;

      @BindView(R.id.toolbar)
     Toolbar toolbar;

      @BindView(R.id.progress_activity_content)
      ProgressBar progressBar;

      @BindView(R.id.rv_main_content_category)
    RecyclerView recyclerView;


      private  CategoryAdapter categoryAdapter;
      private  List<AllCategoryModel.Category> categories;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar = findViewById(R.id.toolbar);
        setContentView(R.layout.activity_category);

        //Butterknife
        ButterKnife.bind(this);

        //set toolbar as action bar
        setSupportActionBar(toolbar);



        //init ListCategory
        //categories is still empty
        categories = new ArrayList<>();

        //Initialise adapter
        categoryAdapter = new CategoryAdapter(CategoryActivity.this,categories);

        //Recycler View
        recyclerView.setLayoutManager(new LinearLayoutManager(CategoryActivity.this));

        //Adapter
        recyclerView.setAdapter(categoryAdapter);

        fetchCategories();
    }


    private void fetchCategories()
    {
        //Write code to fetch all the categories from Mysql with the help of retrofit
        //1. Response class
        //2. Request Interface
        //3. Retrofit's own implementation

        //show progress Bar
        progressBar.setVisibility(View.VISIBLE);

        MyCityKothrudClient myCityKothrudClient=
                new RetrofitService().getRetrofit().create(MyCityKothrudClient.class);



        Call<AllCategoryModel> requestCall = myCityKothrudClient.getAllCategories();

        requestCall.enqueue(new Callback<AllCategoryModel>() {
            @Override
            public void onResponse(Call<AllCategoryModel> call, Response<AllCategoryModel> response)
            {
                //hide progress Bar
                progressBar.setVisibility(View.INVISIBLE);
                AllCategoryModel allCategoryModel =  response.body();

                //check if allCategory model is null
                if(allCategoryModel == null)
                {
                    showSnackbar("Server return null.Please Try Again");
                    return;
                }
                boolean success= allCategoryModel.isSuccess();
                if(success)
                {
                    String message= allCategoryModel.getMessage();
                    List<AllCategoryModel.Category> categoriesList = allCategoryModel.getCategoryList();

                    //Fill the global list 'categories' with this local list 'categoriesList'
                    categories.addAll(categoriesList);

                    //you have to tell the adapter that List is now filled with data
                        categoryAdapter.notifyDataSetChanged();

                    showSnackbar(message + "and size is " + categoriesList.size());
                }
                else
                {
                    showSnackbar(allCategoryModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AllCategoryModel> call, Throwable throwable)
            {
                //hide progress Bar
                progressBar.setVisibility(View.INVISIBLE);
                showSnackbar("Error:"+throwable.getMessage());
            }
        });


    }


    @OnClick(R.id.fab)
    public void  onFabClick()
    {
        Snackbar.make(currentView,"See your Action",BaseTransientBottomBar.LENGTH_LONG).show();
    }

    private void showSnackbar(String message)
    {
        Snackbar.make(currentView,message,BaseTransientBottomBar.LENGTH_LONG).show();
    }


}
