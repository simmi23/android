package com.example.simran.mykothrudcity.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.simran.mykothrudcity.R;
import com.example.simran.mykothrudcity.model.ListingsModel;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListingsAdapter extends RecyclerView.Adapter<ListingsAdapter.ListingViewHolder>
{
    private List<ListingsModel.Listing> listingList;

    public  ListingsAdapter(List<ListingsModel.Listing> listingList)
    {
        this.listingList=listingList;
    }
    @NonNull
    @Override
    public ListingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        LayoutInflater inflater= LayoutInflater.from(viewGroup.getContext());
        View singleItemView=inflater.inflate(R.layout.single_listing_ui,viewGroup,false);

        ListingViewHolder listingViewHolder=new ListingViewHolder(singleItemView);

        return listingViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListingViewHolder listingViewHolder, int i)
    {
        ListingsModel.Listing singleList= this.listingList.get(i);
        String List_name= singleList.getListingName();
        listingViewHolder.txtLNAMe.setText(List_name);

        String List_number= singleList.getListingNumber();
        listingViewHolder.txtLNumber.setText(List_number);

        String List_email= singleList.getListingEmail();
        listingViewHolder.txtLEmail.setText(List_email);

        String List_address= singleList.getListingAddress();
        listingViewHolder.txtLAddress.setText(List_address);

        String List_description= singleList.getListingDesc();
        listingViewHolder.txtLDesc.setText(List_description);

        String List_location= singleList.getListingLocation();
        listingViewHolder.txtLLocation.setText(List_location);


    }


    @Override
    public int getItemCount()
    {
        return this.listingList.size();
    }

    public class  ListingViewHolder extends RecyclerView.ViewHolder
    {
       @BindView(R.id.txt_listing_name_single_listing_ui)
        TextView txtLNAMe;

        @BindView(R.id.txt_listing_number_single_listing_ui)
        TextView txtLNumber;

        @BindView(R.id.txt_listing_email_single_listing_ui)
        TextView txtLEmail;

        @BindView(R.id.txt_listing_address_single_listing_ui)
        TextView txtLAddress;

        @BindView(R.id.txt_listing_location_single_listing_ui)
        TextView txtLLocation;

        @BindView(R.id.txt_listing_description_single_listing_ui)
        TextView txtLDesc;



        public ListingViewHolder(@NonNull View itemView)
        {
            super(itemView);

            //Butterknife
            ButterKnife.bind(this,itemView);
        }
    }
}

