package com.example.simran.mykothrudcity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;


import com.example.simran.mykothrudcity.Adapters.ListingsAdapter;
import com.example.simran.mykothrudcity.client.MyCityKothrudClient;

import com.example.simran.mykothrudcity.model.ListingsModel;
import com.example.simran.mykothrudcity.service.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ListingsActivity extends AppCompatActivity
{

    @BindView(android.R.id.content)
    View currentView;


    //TAG should be less than 23
    private static final String TAG="ListingsActivity";

    @BindView(R.id.toolbar_activity_listings)
    Toolbar toolbar;

    @BindView(R.id.lottie_progress_listings_activity)
    LottieAnimationView lottieAnimationView;

    @BindView(R.id.rv_main_listings_activity)
    RecyclerView recyclerView;

    private ListingsAdapter listingAdapter;
    private List<ListingsModel.Listing> listingsListG;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listings);

        //Init Butterknife
        ButterKnife.bind(this);

       // toolbar= findViewById(R.id.toolbar_activity_listings);



        //Get the category id from categoryAdapter
        Intent callingIntent = getIntent();
        String categoryid= callingIntent.getStringExtra("category_id_key");
        String categoryName = callingIntent.getStringExtra("category_title_key");

        //Set the  category name as title on toolbar
        setSupportActionBar(toolbar);
       toolbar.setTitle(categoryName);
        toolbar.setTitleTextColor(Color.WHITE);

        //init list
        listingsListG=new ArrayList<>();
        //init adapter
        listingAdapter =new ListingsAdapter(listingsListG);
        //code for recycler View
        recyclerView.setLayoutManager(new LinearLayoutManager(ListingsActivity.this));
        //Adapter
        recyclerView.setAdapter(listingAdapter);


        //code to fetch all the listings from api
        Retrofit retrofit= new RetrofitService().getRetrofit();
        //create an objext of MyCityKOthrudClient using retrofit
        MyCityKothrudClient myCityKothrudClient=retrofit.create(MyCityKothrudClient.class);

        //call the getListings(...) method inside of MyCityKothrudClient
        Call<ListingsModel> requestCall =  myCityKothrudClient.getListings(categoryid);

        //call the enqueue method
        requestCall.enqueue(new Callback<ListingsModel>() {
            @Override
            public void onResponse(Call<ListingsModel> call, Response<ListingsModel> response)
            {
                //hide the progress bar(animation)
             lottieAnimationView.cancelAnimation();
             lottieAnimationView.setVisibility(View.INVISIBLE);
                ListingsModel listingsModel= response.body();

                if(listingsModel == null)
                {
                    Snackbar.make(currentView,
                            "Server return nulll.please Try Again" ,
                            BaseTransientBottomBar.LENGTH_INDEFINITE
                            ).show();
                    return;
                }

                boolean success=listingsModel.isSuccess();
                String message= listingsModel.getMessage();
                //check if value of success is true
                if(success)
                {
                 List<ListingsModel.Listing> listingList =   listingsModel.getListingList();

                 //Fill the global list with this local list
                    listingsListG.addAll(listingList);
                  //you have to tell the adapter that list is now filled with data
                    listingAdapter.notifyDataSetChanged();

                 message+="size is"+ listingList.size();

                 Snackbar.make(currentView,
                            message,
                            BaseTransientBottomBar.LENGTH_LONG)
                            .show();

                }
                else
                {
                    Snackbar.make(currentView,
                            message,
                            BaseTransientBottomBar.LENGTH_LONG)
                            .show();
                }

            }

            @Override
            public void onFailure(Call<ListingsModel> call, Throwable throwable)
            {

                //hide the progress bar(animation)
                    lottieAnimationView.cancelAnimation();
                lottieAnimationView.setVisibility(View.INVISIBLE);

                Snackbar.make(currentView,
                        throwable.getMessage(),
                        BaseTransientBottomBar.LENGTH_LONG)
                        .show();

                Log.e(TAG, "onFailure: "+throwable.getMessage(),throwable );
            }
        });

        //Toast.makeText(this, categoryid+ ". " + categoryName, Toast.LENGTH_SHORT).show();
    }
}
