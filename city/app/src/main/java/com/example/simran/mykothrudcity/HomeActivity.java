package com.example.simran.mykothrudcity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity
{

    @BindView(R.id.card_places_activity_home)
    CardView cardView;

              //Declaartion

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);

            ButterKnife.bind(this);

            //Initialisation
            cardView= findViewById(R.id.card_view_news_activity_home);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    //Intent
                    Intent newsIntent= new Intent(HomeActivity.this,NewsActivity.class);
                    startActivity(newsIntent);
                }
            });
    }

    //onclick listener for card places
    @OnClick(R.id.card_places_activity_home)
    public void OnPlacesCardClick()
    {

       // Toast.makeText(this, "places", Toast.LENGTH_SHORT).show();
         Intent categoryList = new Intent(HomeActivity.this,CategoryActivity.class);
         startActivity(categoryList);
    }
}
