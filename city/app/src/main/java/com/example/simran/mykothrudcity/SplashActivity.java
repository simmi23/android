package com.example.simran.mykothrudcity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

//Intent togo from one screen(activity) to another:
//implicit intents :to go from one activity to another in same app
//explicit intents :to go from one activity to another in different app

//recipe to create activity:
//1.java class which extends AppCompatactivity
//2.Layout Xml file
//3.Manifest declaration

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LottieAnimationView animationView=findViewById(R.id.splash_main);
        animationView.addAnimatorListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
               // Toast.makeText(SplashActivity.this, "splash activity ends", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(SplashActivity.this,HomeActivity.class);
                startActivity(intent);
                //This will end current activity(i.e directly come out of app to launcher)
                finish();
            }
        });


    }
}
