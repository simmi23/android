package com.example.simran.mykothrudcity.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListingsModel

{
    @SerializedName("success")
    private boolean success;

    @SerializedName("message")
    private  String message;

    @SerializedName("Listings")
    private List<Listing> listingList;

    public static class Listing
    {
        @SerializedName("id")
        private String listingId;

        @SerializedName("listing_name")
        private String listingName;

        @SerializedName("listing_number")
        private  String listingNumber;

        @SerializedName("listing_email")
        private String listingEmail;

        @SerializedName("listing_address")
        private String listingAddress;

        @SerializedName("listing_desc")
        private String listingDesc;

        @SerializedName("listing_location")
        private String listingLocation;

        @SerializedName("cat_id")
        private String listingCategoryId;

        public String getListingId() {
            return listingId;
        }

        public void setListingId(String listingId) {
            this.listingId = listingId;
        }

        public String getListingName() {
            return listingName;
        }

        public void setListingName(String listingName) {
            this.listingName = listingName;
        }

        public String getListingNumber() {
            return listingNumber;
        }

        public void setListingNumber(String listingNumber) {
            this.listingNumber = listingNumber;
        }

        public String getListingEmail() {
            return listingEmail;
        }

        public void setListingEmail(String listingEmail) {
            this.listingEmail = listingEmail;
        }

        public String getListingAddress() {
            return listingAddress;
        }

        public void setListingAddress(String listingAddress) {
            this.listingAddress = listingAddress;
        }

        public String getListingDesc() {
            return listingDesc;
        }

        public void setListingDesc(String listingDesc) {
            this.listingDesc = listingDesc;
        }

        public String getListingLocation() {
            return listingLocation;
        }

        public void setListingLocation(String listingLocation) {
            this.listingLocation = listingLocation;
        }

        public String getListingCategoryId() {
            return listingCategoryId;
        }

        public void setListingCategoryId(String listingCategoryId) {
            this.listingCategoryId = listingCategoryId;
        }
    }


    //getter and setters for ListingModel
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Listing> getListingList() {
        return listingList;
    }

    public void setListingList(List<Listing> listingList) {
        this.listingList = listingList;
    }
}
