package com.example.simran.fragments.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simran.fragments.R;

public class NowFragment extends Fragment
{
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater,@Nullable ViewGroup container,@Nullable
            Bundle savedInstanceState)
    {
        View rootView= inflater.inflate(R.layout.fragment_now,container,false);
        return rootView;
        }

}
