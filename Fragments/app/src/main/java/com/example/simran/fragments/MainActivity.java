package com.example.simran.fragments;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.simran.fragments.Fragments.NextFragment;
import com.example.simran.fragments.Fragments.NowFragment;
import com.example.simran.fragments.Fragments.UpcomingFragment;

public class MainActivity extends AppCompatActivity
{
        Button btnNow,btnNext,btnUpcoming;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BindView();

        btnNow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                NowFragment nowFragment=new NowFragment();

               FragmentManager cook =getSupportFragmentManager();

               cook
                       .beginTransaction()
                       .replace(R.id.fr_container_activity_main,nowFragment)
                        .commit();

                // Toast.makeText(MainActivity.this, "Now clicked", Toast.LENGTH_SHORT).show();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)

            {
                NextFragment nextFragment= new NextFragment();

                FragmentManager cook=getSupportFragmentManager();
                cook
                        .beginTransaction()
                        .replace(R.id.fr_container_activity_main,nextFragment)
                        .commit();


                //Toast.makeText(MainActivity.this, "Next clicked", Toast.LENGTH_SHORT).show();
            }
        });

        btnUpcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpcomingFragment upcomingFragment=new UpcomingFragment();

                FragmentManager cook=getSupportFragmentManager();
                cook
                        .beginTransaction()
                        .replace(R.id.fr_container_activity_main,upcomingFragment)
                        .commit();
                //Toast.makeText(MainActivity.this, "Upcoming clicked", Toast.LENGTH_SHORT).show();
            }
        });


    }
    public void BindView()
    {
        //Can you create activity Dynamically?
        //NO, it is impossible.

        //An activity is always full screen

        //with the help of fragments
        //Fragment's size can be customized.
        //They can be created in runtime.
        //Fragment is a Sub-Activity

        //Receipe to create a Fragment
        //1. Java class that extends Fragment
        //2. XML Layout file for that Fragment
        btnNow=findViewById(R.id.btn_now_activity_main);
        btnNext=findViewById(R.id.btn_next_activity_main);
        btnUpcoming=findViewById(R.id.btn_upcoming_activity_main);

    }
}
