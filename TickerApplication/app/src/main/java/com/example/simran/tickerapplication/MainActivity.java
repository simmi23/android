package com.example.simran.tickerapplication;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    String textTicker;
        private int count=0;
        private TextView TextTicker;
        private Button btnTickerClick,btnTickerReset,btnTickerIncrement;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        onclick();
        increment();
        resetClick();

    }

    private void showAlert()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setTitle(R.string.reset_alert_title);
        builder.setMessage("Are you sure you want to rseet");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                 updateCount(0);
            }
        });
        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


    }

    
    public void bindViews()
    {
        TextTicker=findViewById(R.id.txt_main_ticker);
        btnTickerClick=findViewById(R.id.btn_main_ticker_click);
        btnTickerReset=findViewById(R.id.btn_main_ticker_reset);
        btnTickerIncrement=findViewById(R.id.btn_main_ticker_increment_two);
        textTicker =TextTicker.getText().toString();

    }

    public void onclick()
    {
        btnTickerClick.setOnClickListener

                (
                        new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                updateCount(count++);
            }
        });
    }

    public void resetClick()
    {
      btnTickerReset.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v)
          {
              resetCount();
          }
      });

    }

    public void increment()
    {
        btnTickerIncrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementCount();
            }
        });

    }

    private void updateCount(int newcount)
    {
        count= newcount;
        TextTicker.setText(String.valueOf(count));
        SharedPreferences sharedPreferences=getSharedPreferences("mypref",MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();

       //addind or updating values to sharedprefernces
        editor.putInt("count", 0);
        editor.apply(); //commit()-same as apply

        ///remove from sharedprefernces//for delete editoe.remove(key name);
        //editor.remove("name");
        //editor.apply();

        //to read from sharedpref
        //sharedPreferences.getString("name","");*/
    }
    private void incrementCount()
    {
        count+=2;
        TextTicker.setText(String.valueOf(count));
    }

    private void resetCount()
    {
        count=0;
        TextTicker.setText(String.valueOf(count));
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }



}
