package com.example.simran.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method will load the respected XML menu file to current Activity
     *
     * @param menu The menu that is being loaded
     * @return Always return true to load the XML file properly
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_notify,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int menuId = item.getItemId();

        switch (menuId) {
            case R.id.Settings_menu_notify: {
                Toast.makeText(this, "Settings Clicked", Toast.LENGTH_SHORT).show();
                return true;
            }

            case R.id.AboutUs_menu_notify: {
                Toast.makeText(this, "AboutUs Clicked", Toast.LENGTH_SHORT).show();
                return true;
            }

            case R.id.Show_notification_menu_notify:
            {
                generateNotification();
                return true;
            }



        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This method will generate a notification
     * To generate any notification , use this method or code inside this method
     */
    private void generateNotification()
    {
        //NotificationManager to show or hide the notification
        NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "default_channel";
        NotificationCompat.Builder nBuilder;
        //How to target a code version specific?
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String channelName= "default_channel";
            int channelImportance= NotificationManager.IMPORTANCE_HIGH;

            //Create a notification channel
            NotificationChannel notificationChannel =new NotificationChannel(channelId,channelName,channelImportance);

            //Create a channel
            if(notificationManager != null)
            notificationManager.createNotificationChannel(notificationChannel);

            //generate a single notification , so that NotificationManager can show it
           nBuilder = new NotificationCompat.Builder(MainActivity.this,channelId);
        }
        else
        {
            //generate a single notification , so that NotificationManager can show it
            nBuilder = new NotificationCompat.Builder(MainActivity.this);
        }

        //Vibration pattern
        long[] vibrationPattern = {500,500,500,500};

        //URI Object
         Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

         //Bitmap object
        Bitmap bitmap= BitmapFactory.decodeResource(getResources(),R.drawable.notification_big);
        Bitmap bitmapLargeIcon= BitmapFactory.decodeResource(getResources(),R.drawable.ic_satellite_black_24dp);


        //3 mandatory parameters
        //title,comtent, SmallIcon
        nBuilder.setContentTitle("Mere Pyare Deshvasiyo");
        nBuilder.setContentText("Atm Closed");
        nBuilder.setSmallIcon(R.drawable.ic_local_atm_black_24dp);
        nBuilder.setColor(Color.RED);
        nBuilder.setOngoing(true);
        nBuilder.setVibrate(vibrationPattern);
        nBuilder.setLights(Color.RED,500,500);
        nBuilder.setSound(uri);
        nBuilder.setStyle(new NotificationCompat.BigPictureStyle()
         .bigPicture(bitmap)
        .setBigContentTitle("big title")
        );
        nBuilder.setLargeIcon(bitmap);

        //click Listener for notification
        Intent notificationIntent= new Intent(MainActivity.this,MainActivity.class);
        PendingIntent pendingIntent =  PendingIntent.getActivity(MainActivity.this,3132,notificationIntent,0);
        nBuilder.setContentIntent(pendingIntent);


        //Show the notification
        Notification notification = nBuilder.build();

        if(notificationManager != null)
            notificationManager.notify(3131,notification);




    }
}
