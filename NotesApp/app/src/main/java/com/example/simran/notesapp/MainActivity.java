package com.example.simran.notesapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.simran.notesapp.dao.Notesdao;
import com.example.simran.notesapp.database.Notesdatabase;
import com.example.simran.notesapp.entity.NotesEntity;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    public static final int ADD_NOTE_REQUEST_CODE = 5253;
    ProgressBar progressBar;
    FloatingActionButton floatingActionButton;
    RecyclerView recyclerView;

    List<NotesEntity> List = new ArrayList<>();

    NotesAdapter notesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                startActivityForResult(intent, ADD_NOTE_REQUEST_CODE);
            }
        });


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        loadNotes();

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Notesdatabase notesdatabase = Notesdatabase.getInstance(MainActivity.this);
                        Notesdao notesdao = notesdatabase.notesdao();
                        notesdao.delete(notesAdapter.getNoteAt(viewHolder.getAdapterPosition()));

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "Note Deleted", Toast.LENGTH_SHORT).show();
//                                finish();
//                                startActivity(getIntent());

                                notesAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
                thread.start();


            }
        }).attachToRecyclerView(recyclerView);


    }

    private void loadNotes() {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                List<NotesEntity> notesEntities = Notesdatabase.getInstance(MainActivity.this).notesdao().getAllNotes();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notesAdapter = new NotesAdapter(notesEntities);
                        recyclerView.setAdapter(notesAdapter);
                        notesAdapter.notifyDataSetChanged();

                    }
                });


            }
        });
        thread1.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_all_notes_menu:
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Notesdatabase notesdatabase = Notesdatabase.getInstance(MainActivity.this);
                        Notesdao notesdao = notesdatabase.notesdao();
                        notesdao.deleteAllNotes();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "All Notes Deleted", Toast.LENGTH_SHORT).show();
                                finish();
                                startActivity(getIntent());
                            }
                        });
                    }
                });
                thread.start();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void bindViews() {
        floatingActionButton = findViewById(R.id.fab_activity_main);
        recyclerView = findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.pbar_main);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ADD_NOTE_REQUEST_CODE) {

            if (resultCode == MainActivity.RESULT_OK) {
                String title = data.getStringExtra("result1");
                String description = data.getStringExtra("result2");
                //NotesResponse n = new NotesResponse();

                //n.setTitle(title);
                //n.setDescription(description);
                //arrayList.add(n);
                //notesAdapter.notifyDataSetChanged();

                /*NotesEntity notesEntity=new NotesEntity(title,description);
                List.add(notesEntity);*/
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        NotesEntity notesEntity = new NotesEntity(title, description);
                        //save data  to sqlite
                        Notesdatabase notesdatabase = Notesdatabase.getInstance(MainActivity.this);

                        Notesdao notesdao = notesdatabase.notesdao();
                        final long result = notesdao.saveNotes(notesEntity);

                        //List<NotesEntity> notesEntities = notesdao.getAllNotes();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (result > 0) {

                                    Toast.makeText(MainActivity.this, "notes saved", Toast.LENGTH_LONG).show();
                                    //notesAdapter = new NotesAdapter(notesEntities);
//                                    finish();
                                    startActivity(getIntent());
                                    //recyclerView.setAdapter(notesAdapter);
                                }
                            }
                        });


                    }

                });
                thread.start();




                /*

                 */

                //Toast.makeText(this, title + " "+description, Toast.LENGTH_SHORT).show();
            }
            /*if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
        }*/
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadNotes();
    }
}

    /*public class GetAllNotes extends AsyncTask<Void, Void, List<NotesEntity>>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<NotesEntity> doInBackground(Void... voids)
        {

                List<NotesEntity> notesEntities=Notesdatabase.getInstance(MainActivity.this).notesdao().getAllNotes();
                return notesEntities;
        }

        @Override
        protected void onPostExecute(List<NotesEntity> notesEntities)
        {
            super.onPostExecute(notesEntities);
            progressBar.setVisibility(View.INVISIBLE);
            if (notesEntities!= null)
            {
                notesEntities.addAll(notesEntities);
                notesAdapter.notifyDataSetChanged();
            }

        }
    }*/







