package com.example.simran.notesapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simran.notesapp.dao.Notesdao;
import com.example.simran.notesapp.database.Notesdatabase;
import com.example.simran.notesapp.entity.NotesEntity;

import java.text.DateFormat;
import java.time.Month;
import java.util.Calendar;
import java.util.List;

public class Main2Activity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener
{
    EditText editTitle,editDescription;
    Button btnAdd,btnDate;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        bindViews();
        btnAdd.setOnClickListener(
                new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String title=editTitle.getText().toString();
                String description=editDescription.getText().toString();

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result1",title);
                returnIntent.putExtra("result2",description);
                setResult(Main2Activity.RESULT_OK,returnIntent);
                finish();
            }
        });
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment=new DatePickerFragment();
                dialogFragment.show(getSupportFragmentManager(),"date picker");
            }
        });



    }
    private void bindViews()
    {
        editTitle=findViewById(R.id.et_title_activity_main2);
        editDescription=findViewById(R.id.et_description_activity_main2);
        btnAdd=findViewById(R.id.btn_add_activity_main2);
     btnDate=findViewById(R.id.date_btn_activity_two);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c=Calendar.getInstance();
        c.set(Calendar.YEAR,year);
        c.set(Calendar.MONTH,month);
        c.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        String currentDate=DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

        TextView txydate=findViewById(R.id.date_activity_two);
         txydate.setText(currentDate);
    }
}
