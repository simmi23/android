package com.example.simran.notesapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.simran.notesapp.entity.NotesEntity;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface Notesdao
{
    @Insert
    long saveNotes(NotesEntity notesEntity);

    @Query("SELECT * FROM tab_notes")
    List<NotesEntity> getAllNotes();

    @Delete
    void delete(NotesEntity notesEntity);

    @Query("DELETE FROM tab_notes ")
    void deleteAllNotes();
}
