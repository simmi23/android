package com.example.simran.notesapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.simran.notesapp.entity.NotesEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NotesViewHolder>
{
    List<NotesEntity> list;


    public NotesAdapter(List<NotesEntity> notesResponses)
    {
        this.list=notesResponses;
    }

    @NonNull
    @Override
    public NotesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        LayoutInflater layoutInflater=LayoutInflater.from(viewGroup.getContext());
        View singleLayoutView= layoutInflater.inflate(R.layout.single_ui,viewGroup,false);
        NotesViewHolder notesViewHolder= new NotesViewHolder(singleLayoutView);
        return notesViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotesViewHolder notesViewHolder, int i)
    {

        NotesEntity singleLayout= list.get(i);

        TextView txt_title= notesViewHolder.title;
        txt_title.setText(singleLayout.getTitle());

        TextView txt_description=notesViewHolder.description;
        txt_description.setText(singleLayout.getDescription());

    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    public NotesEntity getNoteAt(int position)
    {
        return list.get(position);
    }

    public class NotesViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.title_single_ui)
        TextView title;

        @BindView(R.id.description_single_ui)
        TextView description;

        public NotesViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }

}
