package com.example.simran.notesapp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.simran.notesapp.dao.Notesdao;
import com.example.simran.notesapp.entity.NotesEntity;

@Database(entities = NotesEntity.class,version = 1)
public abstract class Notesdatabase extends RoomDatabase
{
    private static final  String DATABASE_NAME="notes_db.db";

    private static Notesdatabase notesdatabase;

    //Instance of dao
    public abstract Notesdao notesdao();

    public static Notesdatabase getInstance(Context context)
    {
        if(notesdatabase == null)
        {
            notesdatabase=Room.databaseBuilder(context,Notesdatabase.class,DATABASE_NAME).build();
        }
       return notesdatabase;
    }

}
