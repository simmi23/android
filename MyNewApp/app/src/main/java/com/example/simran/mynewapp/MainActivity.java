package com.example.simran.mynewapp;

import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar=findViewById(R.id.toolbar_app_bar_main);
        setSupportActionBar(toolbar);

        FloatingActionButton fab=findViewById(R.id.fab_app_bar_main);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Snackbar.make(v,"you clicked on fab",BaseTransientBottomBar.LENGTH_LONG).show();
            }
        });

        DrawerLayout drawer =findViewById(R.id.drawer_activity_main);

        ActionBarDrawerToggle toogle= new ActionBarDrawerToggle(this,drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);

        drawer.addDrawerListener(toogle);
        toogle.syncState();

        NavigationView navigationView=findViewById(R.id.navigation_view_activity_main);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_activity_main);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawermenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.Settings_drawermenu) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
    {
        int id= menuItem.getItemId();
        if(id == R.id.db_drawermenu)
        {
            Toast.makeText(this, "You selected dashboard", Toast.LENGTH_SHORT).show();
        }

        else if(id == R.id.Events_drawermenu)
        {
            Toast.makeText(this, "You selected events", Toast.LENGTH_SHORT).show();
        }

       else  if(id == R.id.Log_out_drawermenu)
        {
            Toast.makeText(this, "You selected log out", Toast.LENGTH_SHORT).show();
        }

        else if(id == R.id.Activities_drawermenu)
        {
            Toast.makeText(this, "You selected activities", Toast.LENGTH_SHORT).show();
        }

        else if(id == R.id.Search_drawermenu)
        {
            Toast.makeText(this, "You selected search", Toast.LENGTH_SHORT).show();
        }

        else if(id == R.id.Settings_drawermenu)
        {
            Toast.makeText(this, "You selected settings", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawerLayout=findViewById(R.id.drawer_activity_main);
        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }
}
