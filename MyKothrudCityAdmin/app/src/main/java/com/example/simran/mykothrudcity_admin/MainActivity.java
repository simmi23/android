package com.example.simran.mykothrudcity_admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
{
        private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

            //Life  Before butterKnife(start)
        button=findViewById(R.id.btn_add_cat_activity_main);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                //Toast.makeText(MainActivity.this, "category added", Toast.LENGTH_SHORT).show();
                //Intent
                Intent categoryIntent=new Intent(MainActivity.this,CategoryActivity.class);
                startActivity(categoryIntent);
            }
        });
        //Life  Before butterKnife(End)

    }

   @OnClick(R.id.btn_add_listings_activity_main)
    public void onListingsButtonClick()
    {
        //Toast.makeText(this, "add listings", Toast.LENGTH_SHORT).show();
        //intent
        Intent listingsIntent=new Intent(MainActivity.this,ListingsCategory.class);
        startActivity(listingsIntent);
    }

}
