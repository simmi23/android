package com.example.simran.mykothrudcity_admin.request;

import com.example.simran.mykothrudcity_admin.response.AddCategoryResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AddCategoryRequest
{
    @FormUrlEncoded()
    @POST("kothrud/api/add_cat_api.php")
    Call<AddCategoryResponse> submitDataToMySql(
          @Field("key_cat_name") String categoryName,
           @Field("key_cat_img_url") String categoryImageUrl
    );

    @FormUrlEncoded()
    @POST("kothrud/api/add_listings_by_cat.php")
    void  addListingsToMySql(
       @Field("key_listing_name") String listingName,
       @Field("key_listing_number") String listingNumber,
       @Field("key_listing_email") String listingEmail,
       @Field("key_listing_address") String listingAddress,
       @Field("key_listing_desc") String listingDesc,
       @Field("key_listing_location") String listingLocation,
       @Field("key_listing_CatId") String listingCategoryId
    );

    //https://kothrud.000webhostapp.com/ -Base URL
    // kothrud/api/get_lis_by_cat.php   -Endpoint
}
