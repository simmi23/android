package com.example.simran.mykothrudcity_admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.example.simran.mykothrudcity_admin.request.AddCategoryRequest;
import com.example.simran.mykothrudcity_admin.response.AddCategoryResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.text.TextUtils.isEmpty;

public class CategoryActivity extends AppCompatActivity
{
          @BindView(R.id.etx_cat_name_activity_category)
          EditText etxCategoryName;

          @BindView(R.id.etx_image_url_activity_category)
          EditText etxImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        //initialise butterknife
        ButterKnife.bind(this);
    }
    @OnClick(R.id.btn_submit_activity_category)
    public void onSubmitClick()

    {
         //get the texts from EditText
        String categoryName=  etxCategoryName.getText().toString();
        String categoryURL=  etxImageUrl.getText().toString();

        //Empty chack validation for Edittext
        boolean isCatNameEmpty=TextUtils.isEmpty(categoryName);
        if(isCatNameEmpty)
        {
            etxCategoryName.setError("Required");
            return;
        }

        //Empty check validation for EdittextImage
        //boolean isCatNameEmpty=TextUtils.isEmpty(categoryName);
        if(TextUtils.isEmpty(categoryURL))
        {
            etxImageUrl.setError("Required");
            return;
        }

        //Hit the api using using retrofit

        //Recipe for cooking retrofit
        //1. A Response Class
        //2. A Request Interface
        //3. Retrofit's Object

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        //Get GSON Converter Factory
        GsonConverterFactory factory=GsonConverterFactory.create();

        //To get Retrofits object we need Retrofits Builder
        Retrofit.Builder retrofitBuilder= new Retrofit.Builder();
        retrofitBuilder.baseUrl("https://kothrud.000webhostapp.com/");


        /*Gson gson = new GsonBuilder()
        .setLenient()
        .create();

Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();*/
        //tell retrofit that we are using Gson as a json parsing librarry
        retrofitBuilder.addConverterFactory(factory);

        //Get Retrofits object from Retrofit.Builder's object
        Retrofit retrofit = retrofitBuilder.build();


        //we need an object of AddCategoryRequest
        //why? To call a method inside of AddCategoryRequest
        //so use Retrofit's Object

        AddCategoryRequest request = retrofit.create(AddCategoryRequest.class);

        //Call  which is inside AddCategoryRequest's object
        Call<AddCategoryResponse> requestCall =
                request.submitDataToMySql(categoryName, categoryURL);

        //perform an Actual Call
        //new<space> ctrl+space
        requestCall.enqueue(new Callback<AddCategoryResponse>() {
            @Override
            public void onResponse(Call<AddCategoryResponse> call, Response<AddCategoryResponse> response) 
            {
             AddCategoryResponse addCategoryResponse= response.body ();

             if (addCategoryResponse == null)
                 return;

            boolean success= addCategoryResponse.isSuccess();
            if(success)
            {
                String message = addCategoryResponse.getMessage();
                Toast.makeText(CategoryActivity.this, message, Toast.LENGTH_SHORT).show();
            }
            }

            @Override
            public void onFailure(Call<AddCategoryResponse> call, Throwable throwable)
            {
                Toast.makeText(CategoryActivity.this, "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
