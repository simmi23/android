package com.example.simran.locationmessageapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simran.locationmessageapp.response.ContactEntity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>
{
    private List<ContactEntity> dataList;


    public ContactsAdapter(List<ContactEntity> dataList1)

    {
        this.dataList = dataList1;

    }

    @NonNull
    @Override
    public ContactsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        LayoutInflater layoutInflater=LayoutInflater.from(viewGroup.getContext());
        View singleLayoutView= layoutInflater.inflate(R.layout.single_ui,viewGroup,false);
        ContactsViewHolder contactViewHolder= new ContactsViewHolder(singleLayoutView);
        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsViewHolder contactsViewHolder, int i)
    {
        ContactEntity singleLayout= dataList.get(i);

        TextView txt_title= contactsViewHolder.textView;
        txt_title.setText(singleLayout.getContactName());

        contactsViewHolder.textView.setBackgroundColor(singleLayout.isSelected() ? Color.argb(80,00,00,00) : Color.WHITE);
        contactsViewHolder.textView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                singleLayout.setSelected(!singleLayout.isSelected());
                contactsViewHolder.textView.setBackgroundColor(singleLayout.isSelected() ? Color.argb(80,00,00,00): Color.WHITE);
            }
        });



        /*if(contact.getSelected() == true)
           {
               holder.checkBox.setChecked(true);
             }*/
    }





    @Override
    public int getItemCount()
    {
        return dataList.size();
        
    }

    public class ContactsViewHolder extends RecyclerView.ViewHolder
    {


        @BindView(R.id.txt_name_single_ui)
        TextView textView;

        public ContactsViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ButterKnife.bind(this,itemView);


        }
    }
}
