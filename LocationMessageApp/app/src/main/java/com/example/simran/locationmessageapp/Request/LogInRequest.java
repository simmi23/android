package com.example.simran.locationmessageapp.Request;

import com.example.simran.locationmessageapp.response.AddSignUpResponse;
import com.example.simran.locationmessageapp.response.LogInResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LogInRequest
{
    @FormUrlEncoded
    @POST("Location/api/Login.php")
     Call<LogInResponse> GetDataFromSignUpToLoginIn(
           @Field("key_user_name") String username,
            @Field("key_pass_word") String password
    );



}
