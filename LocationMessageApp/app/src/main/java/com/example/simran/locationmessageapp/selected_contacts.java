package com.example.simran.locationmessageapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class selected_contacts extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_contacts);

        Intent intent=getIntent();
        String text= intent.getStringExtra("mytext");
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
