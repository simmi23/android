package com.example.simran.locationmessageapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.simran.locationmessageapp.Request.LogInRequest;
import com.example.simran.locationmessageapp.response.LogInResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity
{
    private EditText etxUserName;
    private  EditText etxPassword;
    private Button btnSubmit;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        etxUserName=findViewById(R.id.etx_Username_login_main);
        etxPassword=findViewById(R.id.etx_password_login_main);
        btnSubmit=findViewById(R.id.btn_submitlogin_login_main);
       submitLogIn();



    }

    public void submitLogIn()
    {
        btnSubmit.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String username = etxUserName.getText().toString();
                        String password = etxPassword.getText().toString();

                        //get gson converter  factory
                        GsonConverterFactory factory = GsonConverterFactory.create();

                        //to get Retrofit object we need retrofit's builder
                        Retrofit.Builder retrofitBulider = new Retrofit.Builder();
                        retrofitBulider.baseUrl("https://kothrud.000webhostapp.com/");

                        //tell retrofit we r using gson library
                        retrofitBulider.addConverterFactory(factory);

                        //get retrofit object from Retrofit.Builder object
                        Retrofit retrofit = retrofitBulider.build();


                        LogInRequest request = retrofit.create(LogInRequest.class);

                        Call<LogInResponse> logInResponseCall = request.GetDataFromSignUpToLoginIn(username,password);

                        logInResponseCall.enqueue(new Callback<LogInResponse>()
                        {
                            @Override
                            public void onResponse(Call<LogInResponse> call, Response<LogInResponse> response) {
                                LogInResponse logInResponse = response.body();
                                if (logInResponse == null) {
                                    return;
                                }
                                boolean success = logInResponse.isSuccess();
                                if (success)
                                {
                                    String message = logInResponse.getMessage();
                                    Toast.makeText(Login.this, message, Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(Login.this,afterLogin.class);
                                    startActivity(intent);

                                }
                            }

                            @Override
                            public void onFailure(Call<LogInResponse> call, Throwable throwable)
                            {
                                Toast.makeText(Login.this, "unable to log in"+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }


                });


    }
}