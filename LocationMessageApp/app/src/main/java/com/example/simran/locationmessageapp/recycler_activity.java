package com.example.simran.locationmessageapp;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.simran.locationmessageapp.response.ContactEntity;

import java.util.ArrayList;
import java.util.List;

public class recycler_activity extends AppCompatActivity
{

    ContactsAdapter contactsAdapter;
    ContactEntity contactEntity;
    RecyclerView recyclerView;
    List<ContactEntity> dataList=new ArrayList<>();
    ProgressBar progressBar;
    Button btnAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_activity);
        progressBar=findViewById(R.id.progress_recycler_activity);
        btnAdd=findViewById(R.id.add_activity_recycler);
        recyclerView=findViewById(R.id.rv_activity_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recycler_activity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        ReadContacts readContacts = new ReadContacts();
        readContacts.execute();

        btnAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String text = "";
                for (ContactEntity contactEntity : dataList)
                  {
                       if (contactEntity.isSelected())
                         {
                            text += contactEntity.getContactName();
                         }
                  }
                Intent intent=new Intent(recycler_activity.this,selected_contacts.class);
                intent.putExtra("mytext",text);
                startActivity(intent);
            }
        });



    }


    public class ReadContacts extends AsyncTask<Void, Void, List<ContactEntity>>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<ContactEntity> doInBackground(Void... voids)
        {


            ContentResolver contentResolver = getContentResolver();
            Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);




            while (cursor != null && cursor.moveToNext())
            {
                String id;
                String name = null;
                String email = null;
                String phoneNumber=null;
                contactEntity = new ContactEntity();


                id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                contactEntity.setContactId(id);
                contactEntity.setContactName(name);


                Cursor phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);

                while (phoneCursor != null && phoneCursor.moveToNext())
                {
                    phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    //number = phoneNumber;
                    contactEntity.setFirstNumber(phoneNumber);
                }

                Cursor emailCursor = contentResolver.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + "= ?",
                        new String[]{id},
                        null);

                while (emailCursor != null && emailCursor.moveToNext())
                {
                    String emailAddress = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    email = emailAddress;
                    contactEntity.setContactEmail(email);


                }

               dataList.add(contactEntity);


            }


            return dataList;
    }

    @Override
    protected void onPostExecute(List<ContactEntity> dataList)
    {
        super.onPostExecute(dataList);
        progressBar.setVisibility(View.INVISIBLE);
        if (dataList != null)
        {

            contactsAdapter = new ContactsAdapter(dataList);
            recyclerView.setAdapter(contactsAdapter);
            Log.d("TAG", "name= " + dataList);


        }
    }
//        /**/
        /*Intent intent=new Intent(recycler_activity.this,selected_contacts.class);
        intent.putExtra("mytext",text);
        startActivity(intent);*/





    }
}
