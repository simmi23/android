package com.example.simran.locationmessageapp.response;

import com.google.gson.annotations.SerializedName;

public class AddSignUpResponse
{
    @SerializedName("success")
    private boolean success;

    @SerializedName("message")
    private String message;

    //Getter and setters


    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
