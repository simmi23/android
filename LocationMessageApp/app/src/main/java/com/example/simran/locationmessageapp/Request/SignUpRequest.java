package com.example.simran.locationmessageapp.Request;

import com.example.simran.locationmessageapp.response.AddSignUpResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SignUpRequest
{
    //https://kothrud.000webhostapp.com/-BAse url
    // Location/api/SaveSignUpData.php-end point

    @FormUrlEncoded
    @POST("Location/api/SaveSignUpData.php")
    Call<AddSignUpResponse> submitDataToSignUpTable(
            @Field("key_full_name") String  name,
            @Field("key_number") String number,
            @Field("key_email") String email,
            @Field("key_username") String username,
            @Field("key_password") String password
    );

}
