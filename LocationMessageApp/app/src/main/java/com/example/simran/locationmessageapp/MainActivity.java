package com.example.simran.locationmessageapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.simran.locationmessageapp.Request.SignUpRequest;
import com.example.simran.locationmessageapp.response.AddSignUpResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
{
    EditText etxName;
    EditText etxNumber;
    EditText etxEmail;
    EditText etxUsername;
    EditText etxPassword;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BindViews();
        SignUpButtonListener();


    }

    public void BindViews()
    {
        etxName= findViewById(R.id.etx_name_activity_main);
        etxNumber=findViewById(R.id.etx_phone_activity_main);
        etxEmail=findViewById(R.id.etx_email_activity_main);
        etxUsername=findViewById(R.id.etx_Username_activity_main);
        etxPassword=findViewById(R.id.etx_password_activity_main);
        btnSubmit=findViewById(R.id.btn_submit_activity_main);
    }

    public void SignUpButtonListener()
    {
        btnSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Get the text from edit text
                String  name= etxName.getText().toString();
                String number=etxNumber.getText().toString();
                boolean isNumberEmpty = TextUtils.isEmpty(number);
                if(isNumberEmpty)
                {
                    etxNumber.setError("Required");
                    return;
                }
                String email =etxEmail.getText().toString();
                String username= etxUsername.getText().toString();
                String password=etxPassword.getText().toString();
                //Empty check validation

               //Intent
               //Intent login_intent = new Intent(MainActivity.this,Login.class);
               //startActivity(login_intent);

                //hit the api using retrofit

                //Receipe for cooking retrofit
                //1. A response class
                //2. a request interface
                //3. Retrofit's Object

                //get gson converter  factory
                GsonConverterFactory factory= GsonConverterFactory.create();

                //to get Retrofit object we need retrofit's builder
                Retrofit.Builder retrofitBulider =new Retrofit.Builder();
                retrofitBulider.baseUrl("https://kothrud.000webhostapp.com/");

                //tell retrofit we r using gson library
                retrofitBulider.addConverterFactory(factory);

                //get retrofit object from Retrofit.Builder object
                Retrofit retrofit = retrofitBulider.build();

                //we need an object of AddSignUpRequest
                //to call methid of AddSignUpRequest interface
                SignUpRequest request= retrofit.create(SignUpRequest.class);
                Call<AddSignUpResponse> addSignUpResponseCall = request.submitDataToSignUpTable(name, number, email, username, password);

              addSignUpResponseCall.enqueue(new Callback<AddSignUpResponse>()
              {
                  @Override
                  public void onResponse(Call<AddSignUpResponse> call, Response<AddSignUpResponse> response)
                  {

                      AddSignUpResponse addSignUpResponse = response.body();
                      if (addSignUpResponse==null)
                      {return;}

                      boolean success = addSignUpResponse.isSuccess();
                      if(success)
                      {
                          String message = addSignUpResponse.getMessage();
                          //Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                          Intent login_intent = new Intent(MainActivity.this,Login.class);
                          startActivity(login_intent);

                      }
                  }

                  @Override
                  public void onFailure(Call<AddSignUpResponse> call, Throwable throwable)
                  {
                      Toast.makeText(MainActivity.this, "Error:"+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                  }
              });
            }
        });
    }


}
