package com.example.simran.locationmessageapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class choose extends AppCompatActivity
{
    Button btnSignUp;
    Button btnLogIn;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        btnSignUp=findViewById(R.id.btn_sign_up_activity_choose);
        btnLogIn=findViewById(R.id.btn_log_in_activity_choose);
        ClickListener();

    }
    public void ClickListener()
    {
       btnSignUp.setOnClickListener(new View.OnClickListener()
      {
        @Override
        public void onClick(View v)
        {
            Intent signUpIntent = new Intent(choose.this,MainActivity.class);
            startActivity(signUpIntent);
        }
      });


           btnLogIn.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent logInIntent = new Intent(choose.this, Login.class);
                   startActivity(logInIntent);
               }
           });
    }
}
