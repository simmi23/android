package com.example.simran.recyclerviewrating;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder>
{
        private final static String TAG="DataAdapter";
        private List<Data> datalist;

        public DataAdapter(List<Data> dataList)
        {
            this.datalist= dataList;

        }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        LayoutInflater layoutInflater=LayoutInflater.from(viewGroup.getContext());
        View singleView =layoutInflater.inflate(R.layout.single_aoyout_file,viewGroup,false);

        DataViewHolder dataViewHolder= new DataViewHolder(singleView);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder dataViewHolder, int position)
    {
         Data singleData=datalist.get(position);
         TextView txtTitle= dataViewHolder.title;
         txtTitle.setText(singleData.getTitle());

         TextView txtSubTitle= dataViewHolder.subTitle;
         txtSubTitle.setText(singleData.getSubTitle());

         TextView txtDescription=dataViewHolder.description;
         txtDescription.setText(singleData.getDescription());

         TextView txtTime= dataViewHolder.time;
         txtTime.setText(singleData.getTime());

         Log.d(TAG,"position is"+ position);
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.txt_title_single_layout_file)
        TextView title;

        @BindView(R.id.txt_sub_title_single_layout_file)
        TextView subTitle;

        @BindView(R.id.txt_description_single_layout_file)
        TextView description;

        @BindView(R.id.txt_time_single_layout_file)
        TextView time;

        public DataViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);

        }
    }
}
