package com.example.simran.filedownload;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity
{

    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        mProgressDialog.setMessage("File is being downloaded");
//        mProgressDialog.setIndeterminate(true);
//        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        mProgressDialog.setCancelable(true);

        progressDialog= new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("File is being Downloaded");
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(true);

//        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
//        {
//            @Override
//            public void onCancel(DialogInterface dialog)
//            {
//                downloadManager.
//            }
//        });


        final DownloadManager downloadManager=new DownloadManager(MainActivity.this);
       // downloadManager.execute("https://www.tutorialspoint.com/html/html_tutorial.pdf");
       downloadManager.execute("https://www.tutorialspoint.com/html/html_tutorial.pdf");

    }

    private class DownloadManager extends AsyncTask<String, Integer,String>
    {
        private Context context;
        private PowerManager.WakeLock wakeLock;

        public DownloadManager(Context context1)
        {
            this.context=context1;
        }


        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download

            PowerManager powerManage=(PowerManager) context.getSystemService(Context.POWER_SERVICE);
            wakeLock=powerManage.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,getClass().getName());
            wakeLock.acquire();
            progressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Integer... values)
        {
            super.onProgressUpdate(values);

//            // if we get here, length is known, now set indeterminate to false
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setMax(100);
//            mProgressDialog.setProgress(progress[0]);

            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(values[0]);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            InputStream input = null;
            OutputStream output = null;
            HttpsURLConnection httpsURLConnection=null;


            try
            {
                URL url= new URL(strings[0]);
                httpsURLConnection=(HttpsURLConnection) url.openConnection();
                httpsURLConnection.connect();



                if(httpsURLConnection.getResponseCode() != HttpsURLConnection.HTTP_OK)
                {
                    return "Server return Http" +httpsURLConnection.getResponseCode()+ "" +httpsURLConnection.getResponseMessage();
                }

                int length=httpsURLConnection.getContentLength();

                input = httpsURLConnection.getInputStream();

                output = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Download/html_tutorial.pdf");
//

                byte data[] = new byte[4096];
                long total = 0;
                int count;

                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;

                    // publishing the progress....
                    if (length > 0) // only if total length is known
                        publishProgress((int) (total * 100 / length));
                    output.write(data, 0, count);
                }





            }
            catch (Exception e) {
                return e.toString();
            } finally {
                try
                {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                }
                catch (IOException ignored)
                {

                }


                if (httpsURLConnection != null)
                    httpsURLConnection.disconnect();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);

            wakeLock.release();
            progressDialog.dismiss();
            if (result != null)
                Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG).show();
            else
                Toast.makeText(context,"File downloaded", Toast.LENGTH_SHORT).show();
        }
    }
}
