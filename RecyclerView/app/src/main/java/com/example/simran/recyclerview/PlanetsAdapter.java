package com.example.simran.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlanetsAdapter extends RecyclerView.Adapter<PlanetsAdapter.PlanetViewHolder>
{
            private final static String TAG = "PlanetAdapter";
            private  List<Planet> planetsKIList;

            //constructor
           public PlanetsAdapter(List<Planet>planetList)
           {
                this.planetsKIList = planetList;

           }
    /**
     *
     * This method will bring the single recyclerview item
     * @param viewGroup All the views present inside the
     *                  single_recycler_view
     * @param //viewType  type of Ui
     * @return returns the object of{@link //planetViewHolder}
     */
    @NonNull
    @Override
    public PlanetViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        //in the case where you cannot use setContetView(...) method,
        //use LayoutInflater
        LayoutInflater layoutInflater= LayoutInflater.from(viewGroup.getContext());
        View singleItemView= layoutInflater.inflate(R.layout.single_recyclerview_item,viewGroup,false);

        PlanetViewHolder planetViewHolder=new PlanetViewHolder(singleItemView);
        return planetViewHolder;
    }
    /*
    * This method will be called  getItemCount times
    * @param planetViewHolder object of your single Item view
    * @param position Current position
    * */

    @Override
    public void onBindViewHolder(@NonNull PlanetViewHolder planetViewHolder, int position)
    {
        Planet singlePlanet= planetsKIList.get(position);
      TextView textplanetName=  planetViewHolder.textPlanetName;
      textplanetName.setText(singlePlanet.getPlanetName());

      //Load  image on ImageView with the help of imageUrl.
        //Picasso.get.load(singleParent.getPlanetImageUrl).into(planetViewHolder.imageView)
        Log.d(TAG,"values of position is:"+ position);
    }
/*
* Represents the number of single items in {@link RecyclerView}
*@return  number of single items in int
* */
    @Override
    public int getItemCount()
    {
        return planetsKIList.size();
    }

    public class PlanetViewHolder extends RecyclerView.ViewHolder

    {

        @BindView(R.id.img_single_recyclerview_item)
        ImageView planetimage;

        @BindView(R.id.txt_planet_name_single_recyclerview_item)
        TextView textPlanetName;
        public PlanetViewHolder(@NonNull View itemView)
        {
            super(itemView);

            //butterknife
            ButterKnife.bind(this,itemView);
        }
    }

}
