package com.example.simran.recyclerview;

public class Planet
{
    //planet name
    private String planetName;

    //planet image url
    private String planetImageUrl;


    //Getter an setter(alt+enter)


    public String getPlanetName() {
        return planetName;
    }

    public void setPlanetName(String planetName) {
        this.planetName = planetName;
    }

    public String getPlanetImageUrl() {
        return planetImageUrl;
    }

    public void setPlanetImageUrl(String planetImageUrl) {
        this.planetImageUrl = planetImageUrl;
    }
}
