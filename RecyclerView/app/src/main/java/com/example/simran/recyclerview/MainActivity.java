package com.example.simran.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.LinearLayoutManager.HORIZONTAL;

public class MainActivity extends AppCompatActivity
{

    @BindView(R.id.rv_activity_main)
    RecyclerView recyclerView ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Butterknife
        ButterKnife.bind(this);

        //Init the recyclerView
        //1.LayoutManager
        //2. Adapter

        //LinearLayoutManager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);

        //LinearLayoutManager with orientation
        LinearLayoutManager horizontalLayoutManager =
                       new LinearLayoutManager(MainActivity.this,
                               HORIZONTAL,true);

        //GridLayoutManager
        GridLayoutManager gridLayoutManager=new GridLayoutManager(MainActivity.this,2);

        //StaggeredGridLayoutManager
        StaggeredGridLayoutManager staggeredGridLayoutManager=
                new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.HORIZONTAL);


        //Attach linearLayoutManager to recyclerView
        recyclerView.setLayoutManager(gridLayoutManager);

        // prepare the data
        Planet mercury =  new Planet();
        mercury.setPlanetName("Mercury");
        mercury.setPlanetImageUrl("https://media4.picsearch.com/is?PukAXqnYJ65Kd2-NZ-xH1_hIUFtNnYDA3N72xZ_aVqs&height=341");

        Planet venus = new Planet();
        venus.setPlanetName("Venus");
        venus.setPlanetImageUrl("https://media1.picsearch.com/is?HyBqnfI94OyhWyB_f9LtXnyGu6JkSINbanb18D_yUas&height=224");

         Planet earth = new Planet();
         earth.setPlanetName("Earth");
         earth.setPlanetImageUrl("https://media1.picsearch.com/is?hj6bdj59eG4Vcfm5aKB-wOrF7njUrtNZ4mCeFibGl1I&height=341");

         Planet mars = new Planet();
         mars.setPlanetName("Mars");
         mars.setPlanetImageUrl("www");
        List<Planet> planetList= new ArrayList<>();
        planetList.add(mercury);
        planetList.add(venus);
        planetList.add(earth);
        planetList.add(mars);




        PlanetsAdapter planetsAdapter= new PlanetsAdapter(planetList);

        //Adapter
         recyclerView.setAdapter(planetsAdapter);

    }
}
